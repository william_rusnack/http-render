module Network.HTTP.Render where

import Network.HTTP.Types
import Data.ByteString as B
import Data.ByteString.Char8 as B8
import Data.CaseInsensitive


data Response = Response
  { resHttpVersion :: HttpVersion
  , resStatus :: Status
  , resHeaders :: ResponseHeaders
  }

data ResponseWithBody = ResponseWithBody
  { response :: Response
  , resBody :: ByteString
  }

renderHttpVersion :: HttpVersion -> ByteString
renderHttpVersion (HttpVersion major minor) = "HTTP/" <> int2bs major <> "." <> int2bs minor

renderStatus :: Status -> ByteString
renderStatus (Status code message) = int2bs code <> " " <> message

renderHeader :: Header -> ByteString
renderHeader (hn, v) = original hn <> ": " <> v

renderHeaders :: ResponseHeaders -> ByteString
renderHeaders =
  intercalate "\r\n" .
  fmap renderHeader

renderResponse :: Response -> ByteString
renderResponse res =
  renderHttpVersion (resHttpVersion res) <> " " <>
  renderStatus (resStatus res) <> "\r\n" <>
  renderHeaders (resHeaders res)

renderResponseWithBody :: ResponseWithBody -> ByteString
renderResponseWithBody (ResponseWithBody h b) =
  renderResponse h <> "\r\n" <>
  renderHeader (hContentLength, int2bs $ B.length b) <> "\r\n\r\n" <>
  b

int2bs :: Int -> ByteString
int2bs = B8.pack . show
