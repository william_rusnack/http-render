import Network.HTTP.Render

import Network.HTTP.Types
import Network.HTTP.Types.Header
import Test.Hspec

main :: IO ()
main = hspec $ do
  it "renderHttpVersion" $ do
    renderHttpVersion http09 `shouldBe` "HTTP/0.9"
    renderHttpVersion http10 `shouldBe` "HTTP/1.0"
    renderHttpVersion http11 `shouldBe` "HTTP/1.1"
    renderHttpVersion http20 `shouldBe` "HTTP/2.0"

  it "renderStatus" $ do
    renderStatus continue100 `shouldBe` "100 Continue"
    renderStatus switchingProtocols101 `shouldBe` "101 Switching Protocols"
    renderStatus ok200 `shouldBe` "200 OK"

  it "renderHeaders" $ do
    
    renderHeaders [(hAccept, "audio/*; q=0.2, audio/basic")] `shouldBe` "Accept: audio/*; q=0.2, audio/basic"
    renderHeaders [ (hConnection, "close")
                  , (hContentLength, "3495") ]
      `shouldBe`
        "Connection: close\r\n\
        \Content-Length: 3495"

  it "renderResponse" $ 
    renderResponse Response
      { resHttpVersion = HttpVersion 1 1
      , resStatus = switchingProtocols101
      , resHeaders = [ (hUpgrade, "websocket")
                     , (hConnection, "Upgrade")
                     , ("Sec-WebSocket-Accept", "s3pPLMBiTxaQ9kYGzzhZRbK+xOo=")
                     , ("Sec-WebSocket-Protocol", "chat")
                     ]
      -- , resBody = ""
      } `shouldBe`
        "HTTP/1.1 101 Switching Protocols\r\n\
        \Upgrade: websocket\r\n\
        \Connection: Upgrade\r\n\
        \Sec-WebSocket-Accept: s3pPLMBiTxaQ9kYGzzhZRbK+xOo=\r\n\
        \Sec-WebSocket-Protocol: chat"

  it "renderResponseWithBody adds Content-Length" $ do
    renderResponseWithBody ResponseWithBody
      { response = Response
        { resHttpVersion = http11
        , resStatus = ok200
        , resHeaders = [(hContentType, "text/text")] }
      , resBody = "12345" } `shouldBe`
      "HTTP/1.1 200 OK\r\n\
      \Content-Type: text/text\r\n\
      \Content-Length: 5\r\n\
      \\r\n\
      \12345"

